gulp-remove-future-files
========================

> A *gulp* plugin for removing a file from the stream if it has a future date.

## Setup and usage

Install `gulp-remove-future-files` using `npm`:

```sh
npm i gulp-remove-future-files
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    gulpGrayMatter = require('gulp-gray-matter'),
    gulpRemoveFutureFiles = require('gulp-remove-future-files');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(gulpGrayMatter({ property: "data" }))
    .pipe(gulpRemoveFutureFiles({
        property: "data.date",
        date: new Date(Date.now())
    }))
    .pipe(gulp.dest('./dest'));
});
```

A common workflow using this tool is to remove files that have the `date` YAML header if they occur into the future.

## Options

### date

*string|Date|Moment*

Default: `new Date(Date.now())`

The date to use to determine if a file's date is in the future.

### property

*string*

Default: `data.date`

The dotted notation to retrieve the data inside the file. If the file does not have such a property, the file is included.
