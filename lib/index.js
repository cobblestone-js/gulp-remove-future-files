var through = require('through2');
var dotted = require('dotted');
var moment = require('moment');

module.exports = function(params) {
    // Normalize the values.
    params = params || {};
    params.property = params.property || 'data.date';
    params.date = params.date || new Date(Date.now());

    // Figure out the data threshold.
    var pluginDate = moment(params.date);

    // Create and return the pipe.
    var pipe = through.obj(
        function(file, encoding, callback) {
            // Get the date for the file. If we can't find it, then we pass the
            // file as-is.
            var fileDate = dotted.getNested(file, params.property);

            if (!fileDate) {
                return callback(null, file);
            }

            // See if the date is in the future.
            if (moment(fileDate) <= pluginDate) {
                return callback(null, file);
            }

            // Trigger the callback to continue processing.
            callback();
        });

    return pipe;
};
