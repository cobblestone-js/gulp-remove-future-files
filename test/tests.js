'use strict';

var expect = require('expect');
var plugin = require('../lib/index');
var File = require('vinyl');

function testContents(options, metadata, expected, done) {
    // Create a fake file to test.
    var fakeFile = new File({
        data: metadata,
        contents: Buffer.from('contents'),
        path: 'test.markdown'
    });

    // Pipe it through the plugin.
    var pipe = plugin(options);

    // Count the number of properties.
    var count = 0;

    pipe.on('data', function() {
        count++;
    });
    pipe.on('end', function() {
        expect(count).toBe(expected);
        done();
    });

    // Start the pipe running.
    pipe.write(fakeFile);
    pipe.emit('end');
}

describe('gulp-remove-future-files', function() {
    it('no YAML metadata', function(done) {
        testContents(
            {
                property: 'data.date',
                date: '2017-08-09'
            },
            {},
            1,
            done);
    });

    it('past date', function(done) {
        testContents(
            { date: '2017-08-09' },
            { 'date': '2001-01-01' },
            1,
            done);
    });

    it('past ISO', function(done) {
        testContents(
            { date: '2017-08-09' },
            { 'date': '2001-01-02T01:49:35.776Z' },
            1,
            done);
    });

    it('future date', function(done) {
        testContents(
            { date: '2017-08-09' },
            { 'date': '2020-01-01' },
            0,
            done);
    });

    it('future ISO', function(done) {
        testContents(
            { date: '2017-08-09' },
            { 'date': '2018-01-02T01:49:35.776Z' },
            0,
            done);
    });
});
